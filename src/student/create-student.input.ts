import { InputType, Field } from "@nestjs/graphql";
import { MinLength, IsAlpha } from "class-validator";


@InputType()
export class CreateStudentInput {
    @MinLength(2)
    @IsAlpha()
    @Field()
    firstName: string;

    @MinLength(2)
    @IsAlpha()
    @Field()
    lastName: string;
}